openbookplatform.github.io
==========================

This is a platform designed to:

* Provide access to books licensed under various Open Source Licenses.
* Provide access to books listed in the public domain.


*Responsibility for accessing, downloading and redistributing the software and/or documentation found herein lies with the user. Some books may not be within the public domain in your geographic location.*
